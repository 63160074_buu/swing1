/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener: Action");
    }
    
}

/**
 *
 * @author f
 */
public class HelloMe implements ActionListener {
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(500, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lblYourName = new JLabel("Your Name: ");
        lblYourName.setSize(70, 20);
        lblYourName.setLocation(5, 5);
        lblYourName.setBackground(Color.WHITE);
        lblYourName.setOpaque(true);
        
        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 20);
        txtYourName.setLocation(90, 5);
        
        JButton btnHello = new JButton("Hello");
        btnHello.setSize(80, 20);
        btnHello.setLocation(90, 40);
        
        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener);
        btnHello.addActionListener(new HelloMe());
        
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class: Action");
            }
        };
         btnHello.addActionListener(actionListener);
        
        JLabel lblHello = new JLabel("Hello ...", JLabel.CENTER);
        lblHello.setSize(200, 20);
        lblHello.setLocation(90, 70);
        lblHello.setBackground(Color.WHITE);
        lblHello.setOpaque(true);
        
        frmMain.setLayout(null);
        
        frmMain.add(lblYourName);
        frmMain.add(txtYourName);
        frmMain.add(btnHello);
        frmMain.add(lblHello);
        
        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtYourName.getText();
                lblHello.setText("Hello "+name);
            }
        });
        
        frmMain.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("HelloMe: Action");
    }
}
